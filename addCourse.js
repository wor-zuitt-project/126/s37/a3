let formSubmit = document.querySelector("#createCourse")

formSubmit = addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	let token = localStorage.getItem("token")
	/*
		Create Fetch request to create new course
			- Similar to user registration, except we do not check for a duplicate email
		Make sure that you send the token with thid request
			- Similar to sending the user token in login.js
		If the server responds with "true" for our request, redirect thr
		user to the courses page
		If not, show an error message in an alert
	*/

	
		fetch("http://localhost:4000/courses/",{
					method: 'POST',
					headers: {
							Authorization: `Bearer ${token}`,
							'Content-Type': 'application/json'
						},
					body: JSON.stringify({
						name: courseName,
						description: description,
						price: price
					})	
		})
		.then(res => res.json())
		.then (data => {
			if(data === true){
						
						alert("Successfully create the Course.")
						window.location.replace("./courses.html")
					}else{
						alert("Create Course Fail. Please Try Again")
					}
		})

	


})